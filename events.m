function [value,isterminal,direction] = events(~,Y)

    PARK_SIZE = 110; %meters, varies by stadium
    WALL_CURVE_RADIUS = 30; %meters
    
    x = abs(Y(1));
    y = abs(Y(2));
    
    isterminal = 1;
    %value = y(3);
    
    isBelowFloor = Y(3) <= 0;
    isBeyondStraightWall = max(x, y) > PARK_SIZE;
    
    tx = x - (PARK_SIZE - WALL_CURVE_RADIUS);
    ty = y - (PARK_SIZE - WALL_CURVE_RADIUS);
    isBeyondCurvedWall = and(and(tx>0,ty>0),norm([tx ty]) > WALL_CURVE_RADIUS);
    
    val = or(or(isBelowFloor,isBeyondCurvedWall),isBeyondStraightWall);
    
    if val
        value = 1;
    else
        value = -1;
    end
    
    direction = 1;
end

