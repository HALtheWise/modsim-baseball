function p = parameters()
%PARAMETERS Summary of this function goes here
%   Detailed explanation goes here


    p.r = 0.03638; % radius of ball (m)
    p.m = 0.1417; %mass of ball (kg)
    p.rho = 1.275; %density of air, kg/m^3
    p.Cd = 1; %drag coefficient
    p.eta = 1.845e-5; %Viscosity of air ( m^2/s )
    p.g = 9.81; %gravity, m/s^2
    
    
    p.pitchSpeed = 150; % m/s, can be overridden
    p.theta_xy = 30 * pi/180; %radians, can be overridden
    p.theta_xz = 45 * pi/180; %radians, can be overridden
end

