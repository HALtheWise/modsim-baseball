function [ flows ] = slopefield( t, y, params )
%SLOPEFIELD Summary of this function goes here
%   Detailed explanation goes here
    %P = y(1:3);
    V = y(4:6);
    Omega = y(7:9);
    
    p=params;

    flows = zeros(9,1);

    dV = (1 / p.m) * (-p.m * p.g * [0; 0; 1] - (0.5 * norm(V.^2)) * ...
        p.rho * p.Cd * pi * p.r^2 * V ./ norm(V) + (8 * pi/3) * p.r^3 * p.rho * ...
        cross(Omega,V));
    
    dOmega = (1/(2 * p.m * p.r^2)) * (-8 * pi * p.r^3 * p.eta);
    

    flows(1:3) = V;
    flows(4:6) = dV;
    flows(7:9) = dOmega;
    
end

