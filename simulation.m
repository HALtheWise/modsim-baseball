p = parameters();

P0 = [0;0;0];
V0 = p.pitchSpeed*...
    [cos(p.theta_xy); sin(p.theta_xy); sin(p.theta_xz)];
Omega0 = [0;0;100];

Y0 = cat(1, P0, V0, Omega0 );

simDuration = 10; %seconds

options = odeset('Events', @events);

[Times, Ys] = ode45(@(t,y) slopefield(t,y,p), [0 simDuration], Y0, options);
plot3(Ys(:,1), Ys(:,2), Ys(:,3));
grid on